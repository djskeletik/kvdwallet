from aiogram.dispatcher.filters.state import StatesGroup, State

class Wallet(StatesGroup):
    Start = State()
    Before_transactions = State()
    Before_Course = State()
    Transactions = State()
    Transactions_plus = State()
    Transactions_minus= State()
    Course = State()
    Cryptocourse = State()
    Chosen_of_course = State()
    Convertion = State()
    Сhosen_settings = State()
    Graph_state1 = State()
