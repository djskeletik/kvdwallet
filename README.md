# KVDwallet



## Project founders: Daniil Teslyuk, Vasily Maslov, Ekaterina

## Get started 
To start the bot type the command /start.
All functions are connected in the bot with buttons, so there are no commands. 
Also, all the functionality of the bot is written in Russian. To create a bot, you need to create a bot 
in Telegram from BotFather, get a token and insert it in place of the `API_TOKEN`.
You also need to find an [API](https://currate.ru/) for currency conversion.
```
bot = Bot(token=config.API_TOKEN)
```

