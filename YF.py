from yahoofinancials import YahooFinancials
import matplotlib.pyplot as plt


def get_now_price(currency):
    if len(currency) == 3:
        currency = currency + "-USD"
    cur = YahooFinancials(currency)
    return cur.get_current_price()


def data_optimizer(data):
    months = {"01": "Jan", "02": "Feb", "03": "Mar", "04": "Arp", "05": "May", "06": "June", "07": "July",
              "08": "Aug", "09": "Sept", "10": "Oct", "11": "Nov", "12": "Dec"}
    return data.replace(data[5] + data[6], months[data[5] + data[6]])


def make_graph(m1, m2, text):
    fig, ax = plt.subplots()
    fig.set_size_inches(16, 8)
    ax.plot(m1, m2, linewidth=5, color='#16ab19')
    ax.grid(True)
    fig.set_facecolor('#575757')
    ax.set_facecolor('#9e9e9e')
    ax.set_title(text, fontsize=14, fontweight='bold')
    for label in range(0, len(ax.get_xticklabels(which='major'))):
        ax.get_xticklabels(which='major')[label].set(rotation=30, horizontalalignment='right')
    plt.savefig(text + ".png")
    plt.show()


def get_long_currency(currency, first_date, last_date, step):
    prices_ = []
    dates = []
    if len(currency) == 3:
        currency = currency + "-USD"
    yahoo_financials = YahooFinancials(currency)
    prices = yahoo_financials.get_historical_price_data(first_date, last_date, step)
    for i in prices[currency]['prices']:
        dates.append(data_optimizer(i['formatted_date']))
        prices_.append(i['close'])
        print(str(i['close']) + " " + i['formatted_date'])
    make_graph(dates, prices_, currency)
