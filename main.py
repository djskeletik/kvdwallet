import json
import aiohttp

import YF
import config
import pycoingecko
from aiogram import Bot, Dispatcher, types
from aiogram.types import Message, CallbackQuery, ReplyKeyboardMarkup
from utils import Wallet
from aiogram.dispatcher import FSMContext

import logging
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.contrib.middlewares.logging import LoggingMiddleware
from aiogram.utils import executor
from aiogram_calendar import dialog_cal_callback, DialogCalendar
from config import CURRENCY

bot = Bot(token=config.API_TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())
dp.middleware.setup(LoggingMiddleware())
cg = pycoingecko.CoinGeckoAPI()
logging.basicConfig(level=logging.INFO)
markup = ReplyKeyboardMarkup(resize_keyboard=True)
Currency_for_graph = None
first_date = None
end_date = None


# @dp.message_handler(commands="money")

@dp.message_handler(commands='start')
async def start(message: types.Message, state: FSMContext):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    markup.add("Узнать курс", "Транзакции", "Настройки")
    await message.reply(
        "Здравсвтвуйте, вы попали в KVDwallet, онлайн-кошелек, который исполняет желания.\nНесмотря на все ваши "
        "отрицания он может правда исполнить ваши желания. Давайте так, я привожу два примера исполнения желания, "
        "а вы падаетет со стула, договрились ? Ну давайте. \n \nПервое: для пользования нашим кошелькем и совершения "
        "транзакций на любые суммы совсем не неужен паспорт.\n \nВторое: наш онлайн-кощелек совсем не берет комиссию "
        "при пополнении, снятии и вскоре при переводе в другую купюру.  ",
        reply_markup=markup)
    await Wallet.Start.set()


@dp.message_handler(state=Wallet.Start)
async def first_lobby(message: types.Message, state: FSMContext):
    answer = message.text
    if answer == "Транзакции":
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("Снять", "Положить", "Выйти")
        await message.reply("Вы можете снять или положить деньги", reply_markup=markup)
        await Wallet.Transactions.set()
    elif answer == "Узнать курс":
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.row("Узнать курс криптовалюты", "Узнать курс валюты", "График курса")
        markup.row("Выйти")
        await message.reply("Какой валюты вы хотите узнать курс?", reply_markup=markup)
        await Wallet.Chosen_of_course.set()
    elif answer == "Настройки":
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.row("Перевести мой баланс в другую валюту", "Узнать баланс своего кошелька")
        markup.row("Выйти")
        await message.reply("Что вы хотите выбрать в настройках ?", reply_markup=markup)
        await Wallet.Сhosen_settings.set()

    elif answer == "Выйти":
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("Узнать курс", "Транзакции", "Настройки")
        await message.reply("Вы вышли из раздела 'Настройки'", reply_markup=markup)
        await Wallet.Start.set()

    else:
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("Узнать курс", "Транзакции", "Настройки")
        await message.reply("Вы написали невыполнимую программу, повторите поппытку", reply_markup=markup)
        await Wallet.Start.set()


@dp.message_handler(state=Wallet.Сhosen_settings)
async def chosen_of_course(message: types.Message, state: FSMContext):
    answer = message.text
    if answer == "Перевести мой баланс в другую валюту":
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("EUR", "RUB", "GBP")
        await message.reply(
            "Напишите  валюту ,в которую вам надо перевести ваш баланс\nПример: RUB (перевести сумму вашего кошелька в "
            "рубли)\nИли же нажмите любую из представленных ниже валют, чтобы перевести ваш баланс в эту валюту",
            reply_markup=markup)
        await Wallet.Convertion.set()
    elif answer == "Выйти":
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("Узнать курс", "Транзакции", "Настройки")
        await message.reply("Вы вышли из раздела 'Настройки'", reply_markup=markup)
        await Wallet.Start.set()
    elif answer == "Узнать баланс своего кошелька":
        async with state.proxy() as data:
            if data.get('check'):
                await message.reply("Ваш счет равен: " + str(data['check']) + " " + CURRENCY,
                                    reply_markup=types.ReplyKeyboardRemove())
            else:
                data['check'] = 0
                await message.reply("Ваш счет равен: " + str(data['check']) + " " + CURRENCY,
                                    reply_markup=types.ReplyKeyboardRemove())
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.row("Перевести мой баланс в другую валюту", "Узнать баланс своего кошелька")
        markup.row("Выйти")
        markup.add()
        await message.answer("Что вы хотите выбрать в настройках ?", reply_markup=markup)
        await Wallet.Сhosen_settings.set()

    else:
        await message.reply("Вы написали невыполнимую программу, повторите поппытку")
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.row("Перевести мой баланс в другую валюту", "Узнать баланс своего кошелька")
        markup.row("Выйти")
        markup.add()
        await message.answer("Что вы хотите выбрать в настройках ?", reply_markup=markup)
        await Wallet.Сhosen_settings.set()


@dp.message_handler(state=Wallet.Chosen_of_course)
async def chosen_of_course(message: types.Message):
    answer = message.text
    if answer == "Узнать курс криптовалюты":
        await Wallet.Cryptocourse.set()
        await message.reply("Напишите криптовалюту, к которой вам нужен курс\nПример: bitcoin (доллар в "
                            "рубли)", reply_markup=types.ReplyKeyboardRemove())
    elif answer == "Узнать курс валюты":
        await Wallet.Course.set()
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("EUR", "RUB", "GBP")
        await message.reply("Напишите две валюты без пробела, курс между которыми вам нужен\nПример: USDRUB (доллар в "
                            "рубли)\nИли же нажмите любую из представленных ниже валют, чтобы узнать курс к доллару",
                            reply_markup=markup)
    elif answer == "График курса":
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("EUR", "RUB", "BTC")
        await message.reply("Выбирите график какой валюты, криптовалюты или акции посмтроить?\n"
                            "Пример:\n 1)EUR-USD \n 2)EUR \n 3)AAPL", reply_markup=markup)
        await Wallet.Graph_state1.set()

    elif answer == "Выйти":
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("Узнать курс", "Транзакции", "Настройки")
        await message.reply("Вы вышли из раздела 'Узнать курс'", reply_markup=markup)
        await Wallet.Start.set()
    else:
        await message.reply("Вы написали невыполнимую программу, повторите поппытку")
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("Узнать курс криптовалюты", "Узнать курс валюты", "Выйти")
        await message.answer("Какой валюты вы хотите узнать курс?", reply_markup=markup)
        await Wallet.Chosen_of_course.set()


@dp.message_handler(state=Wallet.Convertion)
async def Convertion(message: types.Message, state: FSMContext):
    print("YES")
    async with aiohttp.ClientSession() as session:
        global CURRENCY
        if message.text is None:
            await message.reply("Ошибка запроса")

        if message.text == "EUR":
            before_currency = message.text
            message.text = CURRENCY + "EUR"
        elif message.text == "RUB":
            before_currency = message.text
            message.text = CURRENCY + "RUB"
        elif message.text == "GBP":
            before_currency = message.text
            message.text = CURRENCY + "GBP"
        else:
            before_currency = message.text
            message.text = CURRENCY + before_currency
        print()
        async with session.get(
                f"https://currate.ru/api/?get=rates&pairs={message.text.upper()}&key={config.MONEY_TOKEN}",
                ssl=False) as resp:
            print(resp)
            json_obj = json.loads(str(await resp.text()))
            print(json_obj)
            if json_obj['message'] == "ooopss":
                await message.reply("Ошибка запроса, некорректно введено название валюты\nПример: USDRUB (доллар в "
                                    "рубли)")
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
                markup.add("EUR", "RUB", "GBP")
                await message.answer(
                    "Напишите валюту без дополнительных значений, \nПример: RUB (первести доллар в "
                    "рубли)\nИли же нажмите любую из представленных ниже валют, чтобы перевести ваш баланс в эту валюту",
                    reply_markup=markup)
                await Wallet.Convertion.set()
            else:
                # bet_markup = types.InlineKeyboardMarkup()
                # button1 = types.InlineKeyboardButton("Перевести деньги в эту валюту", callback_data='www')
                # bet_markup.add(button1)
                async with state.proxy() as data:
                    print(data)
                    if data.get('check'):
                        data['check'] *= float(json_obj["data"][message.text])
                        CURRENCY = before_currency
                        await message.answer("Сумма кошелька переведена на " + CURRENCY,
                                             reply_markup=types.ReplyKeyboardRemove())
                        await message.answer("Ваш счет равен: " + str(data['check']) + " " + CURRENCY,
                                             reply_markup=types.ReplyKeyboardRemove())
                    else:
                        await message.answer("У вас нет пока денег на вашем кошелькк")
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
                markup.row("Перевести мой баланс в другую валюту")
                markup.row("Выйти")
                await message.reply("Что вы хотите выбрать в настройках ?", reply_markup=markup)
                await Wallet.Сhosen_settings.set()


@dp.message_handler(state=Wallet.Graph_state1)
async def simple_cal_handler(message: Message, state: FSMContext):
    currency_for_graph = message.text
    await state.reset_state(with_data=False)
    await message.answer("Выберите дату: ", reply_markup=await DialogCalendar().start_calendar())

# dialog calendar usage
@dp.callback_query_handler(dialog_cal_callback.filter())
async def process_dialog_calendar(callback_query: CallbackQuery, callback_data: dict, state: FSMContext):
    selected, date = await DialogCalendar().process_selection(callback_query, callback_data)
    if selected:
        d = await state.get_data()
        if 'start_date' in d:
            # Ввели вторую дату, можно работать со state
            await state.update_data(end_date=f'{date.strftime("%d.%m.%Y")}')
            d = await state.get_data()
            await callback_query.message.answer(f'Finish, state={d}')
            await state.reset_state(with_data=True)
        else:
            await state.update_data(start_date=f'{date.strftime("%d.%m.%Y")}')
            await callback_query.message.answer(
                f'Вы выбрали: {date.strftime("%d/%m/%Y")}\nВыберите конечную дату:',
                reply_markup=await DialogCalendar().start_calendar())


@dp.message_handler(state=Wallet.Transactions)
async def transaction_selection(message: types.Message, state: FSMContext):
    answer = message.text
    if answer == "Снять":
        await message.reply("Сколько ?", reply_markup=types.ReplyKeyboardRemove())
        await Wallet.Transactions_minus.set()
    elif answer == "Положить":
        await message.reply("Сколько ?", reply_markup=types.ReplyKeyboardRemove())
        await Wallet.Transactions_plus.set()
    elif answer == "Выйти":
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("Узнать курс", "Транзакции", "Настройки")
        await message.reply("Вы вышли из раздела 'Транзакции'", reply_markup=markup)
        await Wallet.Start.set()

    else:
        await message.reply("Вы написали невыполнимую программу, повторите поппытку")
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("Снять", "Положить", "Выйти")
        await message.answer("Вы можете снять или положить деньги в USD", reply_markup=markup)
        await Wallet.Transactions.set()


@dp.message_handler(state=Wallet.Transactions_plus)
async def money_transfer(message: types.Message, state: FSMContext):
    answer = message.text
    before_answer = message.text
    if CURRENCY == 'USD':
        async with state.proxy() as data:
            if data.get('check'):
                data['check'] += int(answer)
                await message.reply(f"{answer} USD положено")
                await message.answer(f"Ваш счет равен: {data.get('check')} {config.CURRENCY}")
            else:
                data['check'] = int(answer)
                await message.reply(f"{answer} USD положено")
                await message.answer(f"Ваш счет равен: {data.get('check')} {config.CURRENCY}")
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("Снять", "Положить", "Выйти")
        await message.answer("Вы можете снять или положить деньги", reply_markup=markup)
        await Wallet.Transactions.set()
    else:
        async with aiohttp.ClientSession() as session:
            message.text = "USD" + CURRENCY
            async with session.get(
                    f"https://currate.ru/api/?get=rates&pairs={message.text.upper()}&key={config.MONEY_TOKEN}",
                    ssl=False) as resp:
                print(resp)
                json_obj = json.loads(str(await resp.text()))
                print(json_obj)
            async with state.proxy() as data:
                answer = int(answer) * float(json_obj["data"][message.text])
                if data.get('check'):
                    data['check'] += answer
                    await message.reply(f"{before_answer} USD (равная {answer} {CURRENCY}) положено")
                    await message.answer(f"Ваш счет равен: {data.get('check')} {CURRENCY}")
                else:
                    data['check'] = int(answer)
                    await message.reply(f"{before_answer} USD (равная {answer} {CURRENCY}) положено")
                    await message.answer(f"Ваш счет равен: {data.get('check')} {CURRENCY}")
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
            markup.add("Снять", "Положить", "Выйти")
            await message.answer("Вы можете снять или положить деньги в USD", reply_markup=markup)
            await Wallet.Transactions.set()


@dp.message_handler(state=Wallet.Transactions_minus)
async def cash_withdrawal(message: types.Message, state: FSMContext):
    answer = message.text
    before_answer = message.text
    if CURRENCY == 'USD':
        async with state.proxy() as data:
            if data.get('check'):
                if data['check'] < int(answer):
                    await message.reply("У вас нe достаточно средств на счету")
                    await message.answer(f"Ваш счет равен: {data.get('check')} {CURRENCY}")
                else:
                    data['check'] -= int(answer)
                    await message.reply(f"{answer} " + CURRENCY + " снято")
                    await message.answer(f"Ваш счет равен: {data.get('check')} {CURRENCY}")
            else:
                data['check'] = 0
                await message.reply("У вас нe достаточно средств на счету")
                await message.answer(f"Ваш счет равен: {data.get('check')} {config.CURRENCY}")
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("Снять", "Положить", "Выйти")
        await message.answer("Вы можете снять или положить деньги в USD", reply_markup=markup)
        await Wallet.Transactions.set()
    else:
        async with aiohttp.ClientSession() as session:
            message.text = "USD" + CURRENCY
            async with session.get(
                    f"https://currate.ru/api/?get=rates&pairs={message.text.upper()}&key={config.MONEY_TOKEN}",
                    ssl=False) as resp:
                print(resp)
                json_obj = json.loads(str(await resp.text()))
                print(json_obj)
        async with state.proxy() as data:
            answer = int(answer) * float(json_obj["data"][message.text])
            curs = float(json_obj["data"][message.text])
            if data.get('check'):
                if data['check'] < int(answer):
                    await message.reply("У вас нe достаточно средств на счету")
                    await message.answer(
                        f"Ваш счет равен: {data.get('check')} {CURRENCY} ({data.get('check') / curs} USD)")
                else:
                    data['check'] -= int(answer)
                    await message.reply(f"{before_answer} USD (равная {answer} {CURRENCY}) снято")
                    await message.answer(f"Ваш счет равен: {data.get('check')} {CURRENCY}")
            else:
                data['check'] = 0
                await message.reply("У вас нe достаточно средств на счету")
                await message.answer(f"Ваш счет равен: {data.get('check')} {config.CURRENCY}")
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("Снять", "Положить", "Выйти")
        await message.answer("Вы можете снять или положить деньги в USD", reply_markup=markup)
        await Wallet.Transactions.set()


@dp.message_handler(state=Wallet.Course)
async def get_course(message: types.Message):
    async with aiohttp.ClientSession() as session:
        if message.text is None:
            await message.reply("Ошибка запроса")
        if message.text == "EUR":
            message.text = "USDEUR"
        if message.text == "RUB":
            message.text = "USDRUB"
        if message.text == "GBP":
            message.text = "USDGBP"
        async with session.get(
                f"https://currate.ru/api/?get=rates&pairs={message.text.upper()}&key={config.MONEY_TOKEN}",
                ssl=False) as resp:
            print(resp)
            json_obj = json.loads(str(await resp.text()))
            print(json_obj)
            if json_obj['message'] == "ooopss":
                await message.reply("Ошибка запроса, некорректно введено название валюты\nПример: USDRUB (доллар в "
                                    "рубли)")
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
                markup.add("EUR", "RUB", "GBP")
                await message.answer(
                    "Напишите две валюты без пробела, курс между которыми вам нужен\nПример: USDRUB (доллар в "
                    "рубли)\nИли же нажмите любую из представленных ниже валют, чтобы узнать их курс к доллару",
                    reply_markup=markup)
                await Wallet.Course.set()
            else:
                bet_markup = types.InlineKeyboardMarkup()
                button1 = types.InlineKeyboardButton("Перевести деньги в эту валюту", callback_data='www')
                bet_markup.add(button1)
                await message.reply("Курс " + message.text + " " + json_obj["data"][message.text])
                markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
                markup.add("Узнать курс криптовалюты", "Узнать курс валюты", "Выйти")
                await message.answer("Какой валюты вы хотите узнать курс?", reply_markup=markup)
                await Wallet.Chosen_of_course.set()


@dp.message_handler(state=Wallet.Cryptocourse)
async def get_cryptocurrency(message: types.Message):
    global currency
    if len(message.text.split()) < 2:
        vs_currencies = 'usd'
        currency = message.text.split()[0]
    elif len(message.text.split()) > 2:
        await message.answer("Ошибка запроса")
    else:
        currency = message.text.split()[0]
        vs_currencies = message.text.split()[0]
    try:
        cryptocurrency = cg.get_price(ids=currency, vs_currencies=vs_currencies)
        await bot.send_message(message.from_user.id,
                               f" Курс: {currency} = {cryptocurrency[currency][vs_currencies]} * {vs_currencies.upper()} ")
    except TypeError:
        await message.answer("Ошибка запроса")
    except KeyError:
        await message.answer("Ошибка запроса")

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    markup.add("Узнать курс криптовалюты", "Узнать курс валюты", "Выйти")
    await message.answer("Какой валюты вы хотите узнать курс?", reply_markup=markup)
    await Wallet.Chosen_of_course.set()


executor.start_polling(dp, skip_updates=True)
